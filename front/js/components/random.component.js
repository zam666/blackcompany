function Random(selector) {
    Component.call(this, selector);
    this.numbers = [];
}

Random.prototype = Object.create(Component.prototype);
Random.constructor = Random;

Random.prototype.init = function() {
    const self = this;
    setInterval(function() {
        axios.get('http://localhost:3000/random-numbers')
            .then(function(response) {
                self.numbers = response.data.data.map(function(number) {
                    return {
                        id: number
                    }
                });

                self.render();
            })
            .catch(function(error) {
                console.error(error);
            });
    }, 10000);
};

Random.prototype.render = function(number) {
    const getListElement = (id, parent) => {
        let $listElement = $listElements[id];
        if (!$listElement) {
            $listElement = document.createElement('li');
            $listElement.classList.add('list-group-item');
            parent.appendChild($listElement);
        }
        return $listElement;
    }
    const container = this.getDOMElement();
    const $listElements = Array.from(container.querySelectorAll('li'));
    this.numbers.forEach((number, id) => {
        const $listElement = getListElement(id, container);
        $listElement.innerText = number.id;
    });
};